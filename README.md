# ExternalControlApplication

ExternalControlApplication is an example program that uses a Olis API to send commands to a robot through OlisOS.

This example program moves the robot in bot joint and cartesian control in a loop.

## Installation

Requirements: 

-Ubuntu 20.04

-ROS noetic installed

-ros-noetic-ros-msg-wrapper-2

To install ros-msg-wrapper-2

```bash
sudo sh -c 'echo "deb [arch=amd64] http://olis-extras.s3.amazonaws.com/debian/ focal main" > /etc/apt/sources.list.d/olis_extras.list'
sudo apt-key adv --keyserver 'hkp://keyserver.ubuntu.com:80' --recv-key 0xE11BD6D4AAE230535A8ADBB321B0333E24AA41DF
sudo apt update
sudo apt install ros-noetic-ros-msg-wrapper-2
```

Clone external_control_application and build

## Usage

```bash
roslaunch external_control_application external_control.launch omc_ip:=192.168.1.93
```
