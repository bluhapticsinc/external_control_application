#include "external_control.h"

ExternalControl::ExternalControl()
{
    actual_joint_state_subscriber_ = node_handle_.subscribe("/external_actual_joint_states",
                                                            100,
                                                            &ExternalControl::actualJointStateCallback,
                                                            this);
    commanded_joint_state_publisher_ = node_handle_.advertise<sensor_msgs::JointState>("/external_commanded_joint_states", 100);

    actual_cartesian_subscriber_ = node_handle_.subscribe("/external_actual_cartesian_position",
                                                          100,
                                                          &ExternalControl::actualCartesianPositionCallback,
                                                          this);
    commanded_cartesian_publisher_ = node_handle_.advertise<std_msgs::Float64MultiArray>("/external_commanded_cartesian_position", 100);

    send_joint_commands_timer_ = node_handle_.createTimer(ros::Duration(0.033), &ExternalControl::sendJointCommands, this);
    send_joint_commands_timer_.start();
    send_cartesian_commands_timer_ = node_handle_.createTimer(ros::Duration(0.033), &ExternalControl::sendCartesianCommands, this);
    send_cartesian_commands_timer_.start();
    connection_monitor_timer_ = node_handle_.createTimer(ros::Duration(0.25), &ExternalControl::disabled, this);
}

void ExternalControl::actualJointStateCallback(const sensor_msgs::JointState &msg)
{
    connection_monitor_timer_.setPeriod(ros::Duration(0.25), true);
    if (starting_joint_angles_.empty() || token_ != std::stoi(msg.header.frame_id))
    {
        reset();
        for (int i = 0; i < msg.name.size(); i++)
            starting_joint_angles_[msg.name.at(i)] = msg.position.at(i);
        token_ = std::stoi(msg.header.frame_id);
    }
}

void ExternalControl::actualCartesianPositionCallback(const std_msgs::Float64MultiArray &msg)
{
    connection_monitor_timer_.setPeriod(ros::Duration(0.25), true);

    if (starting_cartesian_position_.empty())
    {
        starting_cartesian_position_ = std::vector<double>(msg.data.begin(), msg.data.end());
    }
}

void ExternalControl::sendJointCommands(const ros::TimerEvent &e)
{
    if (starting_cartesian_position_.empty() || starting_joint_angles_.empty() || cartesian_mode_)
    {
        sensor_msgs::JointState msg;
        msg.header.frame_id = std::to_string(token_);
        commanded_joint_state_publisher_.publish(msg);
    }
    else
    {
        double move_factor = sin(2 * 3.14159 * static_cast<double>(oscillation_count_++) / static_cast<double>(oscillation_count_max_));
        oscillation_count_ = oscillation_count_ > oscillation_count_max_ ? 0 : oscillation_count_;

        // switches between joint control and cartesian control
        if (oscillation_count_ == oscillation_count_max_)
            cartesian_mode_ = !cartesian_mode_;

        sensor_msgs::JointState msg;
        msg.header.frame_id = std::to_string(token_);
        for (auto joint_angle_it : starting_joint_angles_)
        {

            auto joint_name = joint_angle_it.first;
            auto joint_angle = joint_angle_it.second;
            if (joint_name == "Jaw")
            {
                msg.name.push_back(joint_name);
                msg.position.push_back(getJawAngle(move_factor));
            }
            else
            {
                msg.name.push_back(joint_name);
                msg.position.push_back(joint_angle + 0.2 * move_factor);
            }
        }
        commanded_joint_state_publisher_.publish(msg);
    }
}

void ExternalControl::sendCartesianCommands(const ros::TimerEvent &e)
{
    if (starting_cartesian_position_.empty() || starting_joint_angles_.empty() || !cartesian_mode_)
    {
        std_msgs::Float64MultiArray msg;
        msg.layout.data_offset = token_;
        commanded_cartesian_publisher_.publish(msg);
    }
    else
    {
        double move_factor = sin(2 * 3.14159 * static_cast<double>(oscillation_count_++) / static_cast<double>(oscillation_count_max_));
        oscillation_count_ = oscillation_count_ > oscillation_count_max_ ? 0 : oscillation_count_;

        // switches between joint control and cartesian control
        if (oscillation_count_ == oscillation_count_max_)
            cartesian_mode_ = !cartesian_mode_;

        std_msgs::Float64MultiArray msg;
        msg.layout.data_offset = token_;
        msg.data = starting_cartesian_position_;
        msg.data.at(0) = starting_cartesian_position_.at(0) + 0.1 * move_factor;
        commanded_cartesian_publisher_.publish(msg);

        // move the jaw
        if (starting_joint_angles_.find("Jaw") != starting_joint_angles_.end())
        {
            sensor_msgs::JointState msg;
            msg.header.frame_id = std::to_string(token_);
            msg.name.push_back("Jaw");
            msg.position.push_back(getJawAngle(move_factor));
            commanded_joint_state_publisher_.publish(msg);
        }
    }
}

double ExternalControl::getJawAngle(double move_factor)
{
    double jaw_min = 5;
    double jaw_max = 25;
    return std::min(std::max(starting_joint_angles_.at("Jaw") + 10.0 * move_factor, jaw_min), jaw_max);
}

void ExternalControl::disabled(const ros::TimerEvent &e)
{
    reset();
}

void ExternalControl::reset()
{
    starting_joint_angles_.clear();
    starting_cartesian_position_.clear();
    oscillation_count_ = 0;
}