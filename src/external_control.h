#include <ros/ros.h>

#include <sensor_msgs/JointState.h>
#include <std_msgs/Float64MultiArray.h>

/**
 * @brief This class receives the current robot state and then sends commands oscillating the robots position
 */
class ExternalControl
{
public:
    ExternalControl();

protected:
    /**
     * @brief Receives the actual robot states
     * 
     * msg contians joint names and angles in radians
     * msg token in frame_id
     */
    void actualJointStateCallback(const sensor_msgs::JointState &msg);

    /**
     * @brief Receives the actual robot end effector cartesian position
     * 
     * msg contains a vector of 6 elements xyz in meters and rpy in radians
     * msg token in dim_offset
     */
    void actualCartesianPositionCallback(const std_msgs::Float64MultiArray &msg);

    /**
     * @brief Timer callback used to periodically send joint commands
     * 
     * Commands are only sent once an actual position has been received
     * Commands must include the latest token
     */
    void sendJointCommands(const ros::TimerEvent &e);

    /**
     * @brief Timer callback used to periodically send cartesian commands
     * 
     * Commands are only sent once an actual position has been received
     * Commands must include the latest token
     */
    void sendCartesianCommands(const ros::TimerEvent &e);

    /**
     * @brief Helper function to determine commanded jaw angle for a given move factor
     */
    double getJawAngle(double move_factor);

    /**
     * @brief Timer callback that resets state, and stops sending commands
     * 
     * This timer is restarted everytime a actual position is received, if actual positions 
     * stop coming in it will timeout and call reset
     */
    void disabled(const ros::TimerEvent &e);

    /**
     * @brief Clears state and stops sending commands
     */
    void reset();

    /**
    * OlisOS uses a token to as a handshake to insure that the actual positions have been received before 
    * accepting commanded positions. Therefore all commands should be sent with the latest token.  It's 
    * best practice to reset the commanded positions to the latest actual position whenever a new token 
    * is received
    */
    int token_{-1};

    // flag to toggle between sending cartesian commands and joint state commands
    bool cartesian_mode_{true};

    // starting state stored as a reference position for oscillating commands
    std::map<std::string, double> starting_joint_angles_;
    std::vector<double> starting_cartesian_position_;
    int oscillation_count_ = 0;
    int oscillation_count_max_ = 120;

    ros::NodeHandle node_handle_;
    ros::Timer send_joint_commands_timer_;
    ros::Timer send_cartesian_commands_timer_;
    ros::Timer connection_monitor_timer_;
    ros::Subscriber actual_joint_state_subscriber_;
    ros::Publisher commanded_joint_state_publisher_;
    ros::Subscriber actual_cartesian_subscriber_;
    ros::Publisher commanded_cartesian_publisher_;
};