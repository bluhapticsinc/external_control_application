#include <ros/ros.h>

#include "external_control.h"

int main(int argc, char *argv[])
{
    ros::init(argc, argv, "external_control_application");

    ExternalControl external_control;
    
    ros::spin();

    return 0;
}