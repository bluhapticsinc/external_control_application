cmake_minimum_required(VERSION 3.5)

project(external_control_application)

find_package(catkin REQUIRED
  COMPONENTS
  roscpp
  roslib
  std_msgs
  sensor_msgs
)

catkin_package(
  #INCLUDE_DIRS include 
  LIBRARIES ${PROJECT_NAME}
  CATKIN_DEPENDS roslib roscpp std_msgs sensor_msgs
)

include_directories(${catkin_INCLUDE_DIRS})

add_library(${PROJECT_NAME} src/external_control.cpp)
target_link_libraries(${PROJECT_NAME} ${catkin_LIBRARIES})

add_executable(${PROJECT_NAME}-bin src/main.cpp)
add_dependencies(${PROJECT_NAME}-bin ${PROJECT_NAME})
add_dependencies(${PROJECT_NAME}-bin ${catkin_EXPORTED_TARGETS})
target_link_libraries(${PROJECT_NAME}-bin 
  ${PROJECT_NAME}
)

# rename executable so that executable and binary have the same name
set_target_properties(${PROJECT_NAME}-bin
  PROPERTIES OUTPUT_NAME ${PROJECT_NAME})
